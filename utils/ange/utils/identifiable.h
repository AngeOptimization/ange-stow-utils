#ifndef ANGE_UTILS_IDENTIFIABLE_H
#define ANGE_UTILS_IDENTIFIABLE_H

namespace ange {
namespace utils {

/**
 * Mix-in that gives the inheriting class an ID number for each object
 */
template<typename T>
class Identifiable {

public:

    /**
     * Construct the object with the given ID, or with the next free ID if given id is negative.
     */
    Identifiable(int id) : m_id(id < 0 ? ++s_counter : id) {
        // Empty
    }

    /**
     * Get the ID of this object
     */
    int id() const {
        return m_id;
    }

private:
    static int s_counter;
    int m_id;

};

template<typename T>
int Identifiable<T>::s_counter = 0;

}} // namespace ange::utils

#endif // ANGE_UTILS_IDENTIFIABLE_H
