#ifndef ANGE_UTILS_UNITS_H
#define ANGE_UTILS_UNITS_H

#include <QDebug>
#include <QMetaType>
#include <QString>

#include <cmath>

namespace ange {
namespace units {

/**
 * Define base types allowing compile time unit checks
 * Code somewhat inspired by http://learningcppisfun.blogspot.dk/2007/01/units-and-dimensions-with-c-templates.html
 */
template<int M, int L, int T>
class Unit {

public:

    explicit Unit(double value = 0.0) : m_value(value) {
        // Empty
    }

    Unit(const Unit& rhs) {
        m_value = rhs.m_value;
    }

    // Assignment Operators

    Unit& operator=(const Unit rhs) {
        m_value = rhs.m_value;
        return *this;
    }

    Unit& operator*=(const double rhs) {
        m_value *= rhs;
        return *this;
    }

    Unit& operator/=(const double rhs) {
        m_value /= rhs;
        return *this;
    }

    Unit& operator+=(const Unit rhs) {
        m_value += rhs.m_value;
        return *this;
    }

    Unit& operator-=(const Unit rhs) {
        m_value -= rhs.m_value;
        return *this;
    }

    // Multiplicative and Additive Operators

    double operator/(const Unit rhs) const {
        return m_value / rhs.m_value;
    }

    Unit operator*(const double rhs) const {
        return Unit(m_value * rhs);
    }

    Unit operator/(const double rhs) const {
        return Unit(m_value / rhs);
    }

    Unit operator+(const Unit rhs) const {
        return Unit(m_value + rhs.m_value);
    }

    Unit operator-(const Unit rhs) const {
        return Unit(m_value - rhs.m_value);
    }

    // Equality Operators

    bool operator==(const Unit rhs) const {
        return m_value == rhs.m_value;
    }

    bool operator!=(const Unit rhs) const {
        return m_value != rhs.m_value;
    }

    // Relational Operators

    bool operator<(const Unit rhs) const {
        return m_value < rhs.m_value;
    }

    bool operator>(const Unit rhs) const {
        return m_value > rhs.m_value;
    }

    bool operator<=(const Unit rhs) const {
        return m_value <= rhs.m_value;
    }

    bool operator>=(const Unit rhs) const {
        return m_value >= rhs.m_value;
    }

    // Unary Operators

    Unit operator-() const {
        return Unit(-m_value);
    }

private:
    double m_value;

private:
    template<int M1, int L1, int T1>
    friend Unit<M1, L1, T1> operator*(const double lhs, const Unit<M1, L1, T1> rhs);

    template<int M1, int L1, int T1>
    friend Unit<-M1, -L1, -T1> operator/(const double lhs, const Unit<M1, L1, T1> rhs);

    template<int M1, int L1, int T1, int M2, int L2, int T2>
    friend Unit<M1 + M2, L1 + L2, T1 + T2> operator*(const Unit<M1, L1, T1> lhs, const Unit<M2, L2, T2> rhs);

    template<int M1, int L1, int T1, int M2, int L2, int T2>
    friend Unit<M1 - M2, L1 - L2, T1 - T2> operator/(const Unit<M1, L1, T1> lhs, const Unit<M2, L2, T2> rhs);

    template<int M1, int L1, int T1>
    friend Unit<M1, L1, T1> abs(const Unit<M1, L1, T1> x);

    template <int M1, int L1, int T1>
    friend Unit<2 * M1, 2 * L1, 2 * T1> square(const Unit<M1, L1, T1> x);

};

template<int M, int L, int T>
Unit<M, L, T> operator*(const double lhs, const Unit<M, L, T> rhs) {
    return Unit<M, L, T>(lhs * rhs.m_value);
}

template<int M, int L, int T>
Unit<-M, -L, -T> operator/(const double lhs, const Unit<M, L, T> rhs) {
    return Unit<-M, -L, -T>(lhs / rhs.m_value);
}

template<int M1, int L1, int T1, int M2, int L2, int T2>
Unit<M1 + M2, L1 + L2, T1 + T2> operator*(const Unit<M1, L1, T1> lhs, const Unit<M2, L2, T2> rhs) {
    return Unit<M1 + M2, L1 + L2, T1 + T2>(lhs.m_value * rhs.m_value);
}

template<int M1, int L1, int T1, int M2, int L2, int T2>
Unit<M1 - M2, L1 - L2, T1 - T2> operator/(const Unit<M1, L1, T1> lhs, const Unit<M2, L2, T2> rhs) {
    return Unit<M1 - M2, L1 - L2, T1 - T2>(lhs.m_value / rhs.m_value);
}

template<int M, int L, int T>
Unit<M, L, T> abs(const Unit<M, L, T> x) {
    return Unit<M, L, T>(std::abs(x.m_value));
}

template <int M, int L, int T>
Unit<2 * M, 2 * L, 2 * T> square(const Unit<M, L, T> x) {
    return Unit<2 * M, 2 * L, 2 * T>(x.m_value * x.m_value);
}

typedef Unit<0, 0, 0> Unitless;
typedef Unit<1, 0, 0> Mass;
typedef Unit<0, 1, 0> Length;
typedef Unit<0, 0, 1> Time;
typedef Unit<0, 2, 0> Area;
typedef Unit<0, 3, 0> Volume;
typedef Unit<0, 1, -2> Acceleration;
typedef Unit<1, 1, -2> Force;
typedef Unit<1, 2, -2> Torque;
typedef Unit<1, 1, 0> MassMoment;
typedef Unit<1, -1, 0> LinearDensity;
typedef Unit<1, -3, 0> Density;

// No unit
const Unitless unitless(1.0);

// Mass units
const Mass kilogram(1.0);
const Mass gram = 0.001 * kilogram;
const Mass ton = 1000 * kilogram;

// Length units
const Length meter(1.0);
const Length millimeter = 0.001 * meter;
const Length centimeter = 0.01 * meter;
const Length feet = 0.3048 * meter;

// Area
const Area meter2 = meter * meter;
const Area millimeter2 = millimeter * millimeter;
const Area centimeter2 = centimeter * centimeter;

// Volume
const Volume meter3 = meter * meter * meter;
const Volume millimeter3 = millimeter * millimeter * millimeter;
const Volume centimeter3 = centimeter * centimeter * centimeter;

// Time units
const Time second(1.0);

// Square time
const Unit<0, 0, 2> second2 = second * second;

// Force units
const Force newton = kilogram * meter / second2;
const Force kilonewton = 1000 * newton;

// Torque units
const Torque newtonmeter = newton * meter;
const Torque kilonewtonmeter = kilonewton * meter;

// Mass moment units
const MassMoment kilogrammeter = kilogram * meter;
const MassMoment tonmeter = ton * meter;

// Linear density units
const LinearDensity kilogram_per_meter = kilogram / meter;
const LinearDensity ton_per_meter = ton / meter;

// Density units
const Density kilogram_per_meter3 = kilogram / meter3;
const Density ton_per_meter3 = ton / meter3;
const Density gram_per_centimeter3 = gram / centimeter3;

template<int M, int L, int T>
inline QString symbol() {
    return QStringLiteral("u<%1,%2,%3>").arg(M).arg(L).arg(T);
}

template<>
inline QString symbol<1, 0, 0>() {
    return QStringLiteral("kg");
}

template<>
inline QString symbol<0, 1, 0>() {
    return QStringLiteral("m");
}

template<>
inline QString symbol<0, 0, 1>() {
    return QStringLiteral("s");
}

template<>
inline QString symbol<0, 2, 0>() {
    return QStringLiteral("m^2");
}

template<>
inline QString symbol<0, 3, 0>() {
    return QStringLiteral("m^3");
}

template<>
inline QString symbol<0, 1, -2>() {
    return QStringLiteral("m/s^2");
}

template<>
inline QString symbol<1, 1, -2>() {
    return QStringLiteral("N");
}

template<>
inline QString symbol<1, 2, -2>() {
    return QStringLiteral("Nm");
}

template<>
inline QString symbol<1, 1, 0>() {
    return QStringLiteral("kgm");
}

template<>
inline QString symbol<1, -1, 0>() {
    return QStringLiteral("kg/m");
}

template<>
inline QString symbol<1, -3, 0>() {
    return QStringLiteral("kg/m^3");
}

template<int M, int L, int T>
inline QString toString(ange::units::Unit<M, L, T> x) {
    return QString::number(x / ange::units::Unit<M,L,T>(1)) + symbol<M, L, T>();
}

template<int M, int L, int T>
inline QDebug& operator<<(QDebug& debug, ange::units::Unit<M, L, T> x) {
    debug << toString(x).toUtf8().data();
    return debug;
}

} // namespace units
} // namespace ange

Q_DECLARE_METATYPE(ange::units::Unitless)
Q_DECLARE_METATYPE(ange::units::Mass)
Q_DECLARE_METATYPE(ange::units::Length)
Q_DECLARE_METATYPE(ange::units::Time)
Q_DECLARE_METATYPE(ange::units::Area)
Q_DECLARE_METATYPE(ange::units::Volume)
Q_DECLARE_METATYPE(ange::units::Acceleration)
Q_DECLARE_METATYPE(ange::units::Torque)
Q_DECLARE_METATYPE(ange::units::Force)
Q_DECLARE_METATYPE(ange::units::MassMoment)
Q_DECLARE_METATYPE(ange::units::LinearDensity)
Q_DECLARE_METATYPE(ange::units::Density)

#ifdef QTEST_VERSION
namespace QTest {

template<int M, int L, int T>
inline char* toString(const ange::units::Unit<M, L, T>& x) {
    return toString<QString>(ange::units::toString(x));
}

template<int M, int L, int T>
inline bool qCompare(ange::units::Unit<M, L, T> const &t1, ange::units::Unit<M, L, T> const &t2, const char *actual,
                     const char *expected, const char *file, int line) {
    return compare_helper(qFuzzyCompare(t1/ange::units::Unit<M,L,T>(1), t2/ange::units::Unit<M,L,T>(1)), "Compared values are not the same (fuzzy compare)",
                          QTest::toString(t1), QTest::toString(t2), actual, expected, file, line);
}

} // namespace QTest
#endif // Q_TEST_VERSION

#endif // ANGE_UTILS_UNITS_H
