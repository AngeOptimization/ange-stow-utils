#include <QtTest>

#include "ange/units/units.h"

using namespace ange::units;

class TestUnits : public QObject {
    Q_OBJECT
private Q_SLOTS:
    void testUnits();
    void testToString();
    void testQDebug();
    void testQTestToString();
    void testFuzzy();
};
QTEST_GUILESS_MAIN(TestUnits);

void TestUnits::testUnits() {
    Length x1 = 10 * meter;
    Unit<0, 1, 0> x2 = 5 * meter;
    Unit<0, -1, 0> a = 1 / (x1 + x2);

    // QCOMPARE(a * meter, 1.0/15);  TODO auto convert from Unit<0,0,0> to double
    QCOMPARE(a*meter / unitless, 1.0/15);

    const double b = a/(1/meter);
    QCOMPARE(b, 1.0/15);

    QCOMPARE(-x1, -1*x1);

    QCOMPARE(15*meter, 15*meter);
}

void TestUnits::testToString() {
    QCOMPARE(toString(2.1 * unitless), QStringLiteral("2.1u<0,0,0>"));
    QCOMPARE(toString(2.2 * kilogram), QStringLiteral("2.2kg"));
    QCOMPARE(toString(2.3 * meter), QStringLiteral("2.3m"));
    QCOMPARE(toString(2.4 * second), QStringLiteral("2.4s"));
    QCOMPARE(toString(2.5 * meter2), QStringLiteral("2.5m^2"));
    QCOMPARE(toString(2.6 * meter3), QStringLiteral("2.6m^3"));
    QCOMPARE(toString(2.7 * meter/second2), QStringLiteral("2.7m/s^2"));
    QCOMPARE(toString(2.8 * newton), QStringLiteral("2.8N"));
    QCOMPARE(toString(2.9 * newton*meter), QStringLiteral("2.9Nm"));
    QCOMPARE(toString(3.0 * kilogram*meter), QStringLiteral("3kgm"));
    QCOMPARE(toString(3.1 * kilogram/meter), QStringLiteral("3.1kg/m"));
    QCOMPARE(toString(3.2 * kilogram/meter3), QStringLiteral("3.2kg/m^3"));
    QCOMPARE(toString(4.2 * Unit<7,8,9>(1)), QStringLiteral("4.2u<7,8,9>"));
}

void TestUnits::testQDebug() {
    QString string;
    QDebug debug(&string);
    debug << 2.1 * unitless;
    debug << 2.2 * kilogram;
    debug << 2.3 * meter;
    debug << 2.4 * second;
    debug << 2.5 * meter2;
    debug << 2.6 * meter3;
    debug << 2.7 * meter/second2;
    debug << 2.8 * newton;
    debug << 2.9 * newton*meter;
    debug << 3.0 * kilogram*meter;
    debug << 3.1 * kilogram/meter;
    debug << 3.2 * kilogram/meter3;
    debug << 4.2 * Unit<7,8,9>(1);
    QCOMPARE(string, QStringLiteral("2.1u<0,0,0> 2.2kg 2.3m 2.4s 2.5m^2 2.6m^3 2.7m/s^2 2.8N 2.9Nm "
                                    "3kgm 3.1kg/m 3.2kg/m^3 4.2u<7,8,9> "));
}

void TestUnits::testQTestToString() {
    QCOMPARE(QTest::toString(2.1 * unitless), "\"2.1u<0,0,0>\"");
    QCOMPARE(QTest::toString(2.2 * kilogram), "\"2.2kg\"");
    QCOMPARE(QTest::toString(2.3 * meter), "\"2.3m\"");
    QCOMPARE(QTest::toString(2.4 * second), "\"2.4s\"");
    QCOMPARE(QTest::toString(2.5 * meter2), "\"2.5m^2\"");
    QCOMPARE(QTest::toString(2.6 * meter3), "\"2.6m^3\"");
    QCOMPARE(QTest::toString(2.7 * meter/second2), "\"2.7m/s^2\"");
    QCOMPARE(QTest::toString(2.8 * newton), "\"2.8N\"");
    QCOMPARE(QTest::toString(2.9 * newton*meter), "\"2.9Nm\"");
    QCOMPARE(QTest::toString(3.0 * kilogram*meter), "\"3kgm\"");
    QCOMPARE(QTest::toString(3.1 * kilogram/meter), "\"3.1kg/m\"");
    QCOMPARE(QTest::toString(3.2 * kilogram/meter3), "\"3.2kg/m^3\"");
    QCOMPARE(QTest::toString(4.2 * Unit<7,8,9>(1)), "\"4.2u<7,8,9>\"");
}

void TestUnits::testFuzzy() {
    QCOMPARE(1.0*meter, 1*meter + 1e-13*meter);
    QEXPECT_FAIL(0, "This test should fail, do not accept a 1e-12 deviation", Continue);
    QCOMPARE(1.0*meter, 1*meter + 1e-12*meter);

    QCOMPARE(1.0*second, 1*second + 1e-13*second);
    QEXPECT_FAIL(0, "This test should fail, do not accept a 1e-12 deviation", Continue);
    QCOMPARE(1.0*second, 1*second + 1e-12*second);
}

#include "testunits.moc"
