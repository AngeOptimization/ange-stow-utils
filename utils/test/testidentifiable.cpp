#include <QtTest>

#include "ange/utils/identifiable.h"

#include <QObject>

using ange::utils::Identifiable;

class TestIdentifiable : public QObject {
    Q_OBJECT
private Q_SLOTS:
    void testIdentifiable();
};
QTEST_GUILESS_MAIN(TestIdentifiable);

class A : public Identifiable<A> {
public:
    A(int id = -1) : Identifiable(id) {
        // Empty
    }
};

class B : public Identifiable<B> {
public:
    B(int id = -1) : Identifiable(id) {
        // Empty
    }
};

void TestIdentifiable::testIdentifiable() {
    A a;
    QCOMPARE(a.id(), 1);
    QCOMPARE(A().id(), 2);
    QCOMPARE(A(42).id(), 42);
    QCOMPARE(A(1).id(), 1);
    QCOMPARE(A(-1).id(), 3);

    QCOMPARE(B().id(), 1);

    QCOMPARE(A().id(), 4);

    QCOMPARE(B().id(), 2);

    QCOMPARE(a.id(), 1);
}

#include "testidentifiable.moc"
