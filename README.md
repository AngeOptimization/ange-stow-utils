Utilities used by Ange when building Angelstow
==============================================

These utilities are used by many of the projects that are linked together into
the Angelstow application.

If code could be placed in a more specific project do it!

For now it contains:
  * Units - library that manages physical units

Later it might get:
  * CMake config - shared CMake config

License
-------

Copyright (C) 2010-2015 Ange Optimization ApS <contact@ange.dk>

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

The full GNU General Public License can be found in the file LICENSE.txt.
